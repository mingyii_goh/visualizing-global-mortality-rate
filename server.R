
shinyServer(function(input,ouput,session) {

  
  output$plot <- renderPlot({
    
    #with(dat1[location_nm == input$loc] ,plot(x = dat1$day, y =dat1[,get(input$param)], type = "l"))
    plotDat <- dat1[location_nm == input$loc]
    xvar <- get("day", dat1)
    yvar <- get(input$param, dat1)
    plot(x = xvar, y =yvar, type = "l")
  })

}) 